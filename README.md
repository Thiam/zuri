## About Zuri

Zuri is a connected drone that uses Google's artificial intelligence to receive voice commands from the user. For the latter, Zuri takes the form of an integrated service to the Google assistant. The user can therefore send voice commands to the drone through his smartphone or his Google Home.

## Prerequisites

I strongly recommend Lubuntu >= 16.04 as operating system to run this project on Raspberry Pi 3 (your device has to be bluetooth enabled). But it might also run on any Debian distro that is not older than 2015. You will also have to install the Go programming language. You can follow this link to see how : [https://cloud.google.com/pubsub/docs/overview](https://cloud.google.com/pubsub/docs/overview).

## Installation

Make sure, you install these packages as sudo.

Before you can install Zuri, you need to make sure that you have properly installed the required packages. You can follow these installations guidelines.
In a terminal, do :
- `sudo apt install golang`
- `go get -d -u gobot.io/x/gobot/...`
- Follow [Cloud Pub/Sub installation guideline](https://cloud.google.com/pubsub/docs/quickstart-client-libraries)

## Some Useful links

- [www.gobot.io](http://www.gobot.io)
- [https://cloud.google.com/pubsub/docs/overview](http://https://cloud.google.com/pubsub/docs/overview)

## Security Vulnerabilities

If you discover a security vulnerability within Zuri, please send an e-mail to Mohamed Thiam via [medteck15@gmail.com](mailto:medteck15@gmail.com).

## License

Copyright (c) 2018 Mohamed Thiam. Licensed under the [MIT license](https://opensource.org/licenses/MIT).
