const PubSub = require('@google-cloud/pubsub');
const similarity = require('similarity');
const commands = require('./commands.json');

const pubsub = new PubSub();
const topicName = 'dialog';
const dialogflowSource = process.env.DIALOGFLOW_SOURCE;
const similarityFactor = 0.85;

exports.handle = (req, res) => {
  let commandReceived = req.body.originalDetectIntentRequest.payload.inputs[0].arguments[0].textValue;
  const matchingCommand = commands.find(command => {
    if (similarity(commandReceived, command.text) > similarityFactor) {
      return command;
    }
  });
  if (matchingCommand) {
    const dataBuffer = Buffer.from( matchingCommand.msg );
    let response = {
      source: dialogflowSource
    };
    pubsub
      .topic(topicName)
      .publisher()
      .publish(dataBuffer)
    	.then(messageId => {
        response.fulfillmentText = matchingCommand.response || 'D\'accord, je prépare votre commande.';
        res.status(200).send( JSON.stringify(response) );
    	})
    	.catch(error => {
        response.fulfillmentText = 'Désolé, une erreur s\'est produite lors du traitement de votre commande.';
      	res.status(200).send( JSON.stringify(response) );
    	});
  } else {
    response.fulfillmentText = 'Désolé, je ne suis pas sûr de comprendre';
    res.status(200).send( JSON.stringify(response) );
  }
};
