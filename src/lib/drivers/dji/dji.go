package dji

import (
	"fmt"
	"context"
	"math/rand"
	"time"

	"gobot.io/x/gobot"
    "gobot.io/x/gobot/platforms/dji/tello"

	"cloud.google.com/go/pubsub"

    "../../electron"
)

var robot *gobot.Robot
var once_connected = false
var flying = false
var work func()
var drone *tello.Driver

const (
	// L'identifiant du projet sur Google Cloud Platform
	project_id						= "zuri-b3d9d";
	subscription_name				= "go-monitor";

	// Déplacements
	Rotation_petite = 40;
	Rotation_grande = 60;
	Horizontal_petit = 40;
	Horizontal_grand = 60;
	Vertical_petit = 60;
	Vertical_grand = 60;
	Motion_duration = 2*time.Second;

	// Les Commandes
	Commande_connexion 				= "0";
	Commande_decollage 				= "1";
	Commande_atterrissage 			= "2";
	Commande_backflip 				= "3";
	Commande_avance 				= "4";
	Commande_avance_un_peu 			= "5";
	Commande_recule 				= "6";
	Commande_recule_un_peu 			= "7";
	Commande_gauche 				= "8";
	Commande_gauche_un_peu 			= "9";
	Commande_droite 				= "10";
	Commande_droite_un_peu 			= "11";
	Commande_tourne_droite 			= "12";
	Commande_tourne_droite_un_peu 	= "13";
	Commande_tourne_gauche 			= "14";
	Commande_tourne_gauche_un_peu 	= "15";
	Commande_monte 					= "16";
	Commande_monte_un_peu 			= "17";
	Commande_descend 				= "18";
	Commande_descend_un_peu 		= "19";
	Commande_figure_acrobatique 	= "20";
	Commande_photo	 				= "21";
)

func connect() {
    work = func() {
		drone.On(tello.TakeoffEvent, func(data interface{}) {
			flying = true
		})

		drone.On(tello.LandingEvent, func(data interface{}) {
			flying = false
		})

		once_connected = true
		electron.Output("log", "Connecté au drone!")
	}

    robot := gobot.NewRobot("tello",
        []gobot.Connection{},
        []gobot.Device{drone},
        work,
    )

	if robot == nil {
		fmt.Errorf("Erreur lors de la connexion au drone. Nouvelle tentative..")
		time.Sleep(500 * time.Millisecond)
		connect()
	} else {
		robot.Start()
	}
}

func reconnect() {
    robot := gobot.NewRobot("tello",
        []gobot.Connection{},
        []gobot.Device{drone},
        work,
    )

    robot.Start()
}

func handle_command(ctx context.Context, m *pubsub.Message) {
	electron.Output("commande", m.Data)
	switch string(m.Data) {
		case Commande_connexion:
			if !once_connected {
				drone = tello.NewDriver("8888")
				connect()
			} else if once_connected {
				reconnect()
			}
		case Commande_decollage:
			drone.TakeOff()
		case Commande_atterrissage:
			drone.Land()
		case Commande_backflip:
			drone.BackFlip()
		case Commande_avance:
			drone.Forward(Horizontal_grand)
			gobot.After(Motion_duration, func() {
				drone.Forward(0)
			})
		case Commande_avance_un_peu:
			drone.Forward(Horizontal_petit)
			gobot.After(Motion_duration, func() {
				drone.Forward(0)
			})
		case Commande_recule:
			drone.Backward(Horizontal_grand)
			gobot.After(Motion_duration, func() {
				drone.Backward(0)
			})
		case Commande_recule_un_peu:
			drone.Backward(Horizontal_petit)
			gobot.After(Motion_duration, func() {
				drone.Backward(0)
			})
		case Commande_gauche:
			drone.Left(Horizontal_grand)
			gobot.After(Motion_duration, func() {
				drone.Left(0)
			})
		case Commande_gauche_un_peu:
			drone.Left(Horizontal_petit)
			gobot.After(Motion_duration, func() {
				drone.Left(0)
			})
		case Commande_droite:
			drone.Right(Horizontal_grand)
			gobot.After(Motion_duration, func() {
				drone.Right(0)
			})
		case Commande_droite_un_peu:
			drone.Right(Horizontal_petit)
			gobot.After(Motion_duration, func() {
				drone.Right(0)
			})
		case Commande_tourne_droite:
			drone.Clockwise(Rotation_grande)
			gobot.After(Motion_duration, func() {
				drone.Clockwise(0)
			})
		case Commande_tourne_droite_un_peu:
			drone.Clockwise(Rotation_petite)
			gobot.After(Motion_duration, func() {
				drone.Clockwise(0)
			})
		case Commande_tourne_gauche:
			drone.CounterClockwise(Rotation_grande)
			gobot.After(Motion_duration, func() {
				drone.CounterClockwise(0)
			})
		case Commande_tourne_gauche_un_peu:
			drone.CounterClockwise(Rotation_petite)
			gobot.After(Motion_duration, func() {
				drone.CounterClockwise(0)
			})
		case Commande_monte:
			drone.Up(Vertical_grand)
			gobot.After(Motion_duration, func() {
				drone.Up(0)
			})
		case Commande_monte_un_peu:
			drone.Up(Vertical_petit)
			gobot.After(Motion_duration, func() {
				drone.Up(0)
			})
		case Commande_descend:
			drone.Down(Vertical_grand)
			gobot.After(Motion_duration, func() {
				drone.Down(0)
			})
		case Commande_descend_un_peu:
			drone.Down(Vertical_petit)
			gobot.After(Motion_duration, func() {
				drone.Down(0)
			})
		case Commande_figure_acrobatique:
			switch rand.Intn(3) {
				case 0:
					drone.BackFlip()
				case 1:
					drone.FrontFlip()
				case 2:
					drone.LeftFlip()
				case 3:
					drone.RightFlip()
			}
		default:
			electron.Output("Commande inconnue", m.Data)
	}
	m.Ack()
}

func Initialize() {
    ctx := context.Background()

	// Création d'un client (Subscriber)
	client, err := pubsub.NewClient(ctx, project_id)
	if err != nil {
		fmt.Errorf("Erreur lors de la création du client: %v", err)
	}

	// On utilise la Subscription "go-monitor" déjà créée sur Cloud Pub/Sub
	sub := client.Subscription(subscription_name)

	ok, err := sub.Exists(ctx)
	if err != nil {
		electron.Output("warn", "La subscription n'existe pas.")
	}
	if ok {
		electron.Output("log", "Prêt à recevoir les commandes Pub/Sub!")
	}

    // drone = tello.NewDriver("8888")
	// connect()

	sub.Receive(ctx, handle_command)

	if err != nil {
		fmt.Errorf("Iitialization failed: %v", err)
	}
}
