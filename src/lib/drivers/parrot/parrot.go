package parrot

import (
	"fmt"
	"os"
	"time"
	"context"
	"math/rand"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/platforms/ble"

	"cloud.google.com/go/pubsub"

    "../../electron"
)

var robot *gobot.Robot
var bleAdaptor *ble.ClientAdaptor
var drone *minidrone.Driver
var once_connected = false
var flying = false
var work func()

const (
	// L'identifiant du projet sur Google Cloud Platform
	project_id						= "zuri-b3d9d";

	// Les Commandes
	Commande_connexion 				= "0";
	Commande_decollage 				= "1";
	Commande_atterrissage 			= "2";
	Commande_backflip 				= "3";
	Commande_avance 				= "4";
	Commande_avance_un_peu 			= "5";
	Commande_recule 				= "6";
	Commande_recule_un_peu 			= "7";
	Commande_gauche 				= "8";
	Commande_gauche_un_peu 			= "9";
	Commande_droite 				= "10";
	Commande_droite_un_peu 			= "11";
	Commande_tourne_droite 			= "12";
	Commande_tourne_droite_un_peu 	= "13";
	Commande_tourne_gauche 			= "14";
	Commande_tourne_gauche_un_peu 	= "15";
	Commande_monte 					= "16";
	Commande_monte_un_peu 			= "17";
	Commande_descend 				= "18";
	Commande_descend_un_peu 		= "19";
	Commande_figure_acrobatique 	= "20";
	Commande_photo	 				= "21";
)

func connect(bluetooth_address string) {
	bleAdaptor = ble.NewClientAdaptor(bluetooth_address)
	drone = minidrone.NewDriver(bleAdaptor)

	work = func() {
		drone.On(minidrone.Takeoff, func(data interface{}) {
			flying = true
		})

		drone.On(minidrone.Landed, func(data interface{}) {
			flying = false
		})

		once_connected = true
		electron.Output("log", "Connecté au drone!")
	}

	robot = gobot.NewRobot("minidrone",
		[]gobot.Connection{bleAdaptor},
		[]gobot.Device{drone},
		work,
	)

	robot.Start()

    return robot
}

func reconnect(robot *gobot.Robot) {
	robot = gobot.NewRobot("minidrone",
		[]gobot.Connection{bleAdaptor},
		[]gobot.Device{drone},
		work,
	)
	robot.Start()

    return robot
}

func handle_command(ctx context.Context, m *pubsub.Message) {
	message := string(m.Data)
	switch string(m.Data) {
		case Commande_connexion:
			if bleAdaptor == nil {
				connect(drone_bluetooth_address)
			} else if once_connected {
				reconnect()
			}
		case Commande_decollage:
			drone.TakeOff()
		case Commande_atterrissage:
			drone.Land()
		case Commande_backflip:
			drone.BackFlip()
		case Commande_avance:
			drone.Forward(50)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_avance_un_peu:
			drone.Forward(20)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_recule:
			drone.Backward(50)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_recule_un_peu:
			drone.Backward(20)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_gauche:
			drone.Left(20)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_gauche_un_peu:
			drone.Left(10)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_droite:
			drone.Right(20)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_droite_un_peu:
			drone.Right(10)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_tourne_droite:
			drone.Clockwise(20)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_tourne_droite_un_peu:
			drone.Clockwise(10)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_tourne_gauche:
			drone.CounterClockwise(20)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_tourne_gauche_un_peu:
			drone.CounterClockwise(10)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_monte:
			drone.Up(30)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_monte_un_peu:
			drone.Up(15)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_descend:
			drone.Down(30)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_descend_un_peu:
			drone.Down(15)
			gobot.After(2*time.Second, func() {
				drone.Stop()
			})
		case Commande_figure_acrobatique:
			switch rand.Intn(3) {
				case 0:
					drone.BackFlip()
				case 1:
					drone.FrontFlip()
				case 2:
					drone.LeftFlip()
				case 3:
					drone.RightFlip()
			}
		case Commande_photo:
			drone.TakePicture()
	}
	m.Ack()
}
