package electron

import "fmt"

func Output(ctype string, content interface{}) string {
	fmt.Printf("{\"type\":\"%v\", \"content\": \"%v\"}", ctype, content)
	return ""
}
