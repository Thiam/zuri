package main

import (
	"os"
	"time"

	"./lib/drivers/dji"
	"./lib/electron"
)

const (
	// Les Drivers
	Driver_minidrone				= "minidrone";
	Driver_tello					= "tello"
)

func main() {

	switch os.Args[1] {
		case Driver_tello:
			go forever()
			dji.Initialize()
		default:
			electron.Output("error", "Driver not supported for your drone")
	}

}

func forever() {
    for {
        time.Sleep(time.Minute)
    }
}
